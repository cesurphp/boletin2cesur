<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 24 </title>
    <script src="https://cdn.tailwindcss.com"></script>
</head>

<body class="flex justify-center m-10">
    <div class="max-w-sm rounded overflow-hidden shadow-lg bg-black text-white">
        <div class="px-6 py-4">
            <div class="font-bold text-xl mb-2 text-green-500">Ejercicio 24: Funcion cuenta vocales</div>
            <div>
                <code> 
                   <span class="text-green-500"> function</span> <span class="text-yellow-400">cuentaVocales ($palabra)</span> {<br>
                   <span class="text-blue-300"> $contador = 0;<br>
                    $vocales = array("a","e","i","o","u");<br>
                    for ($i = 0; $i < strlen($palabra); $i++) {<br>
                        if (in_array($palabra[$i], $vocales)) {<br>
                        $contador++;<br>
                        }<br></span>
                        }<br>
                       <p class='text-yellow-400'> echo "El número total en la palabra o frase <u>$palabra</u> es: <br> $contador vocales.";<br>
                        }<br><br></p>

                        <span class="text-green-500"><u>Aplicamos la funcion</u> </span><br>
                       <span class="text-blue-300">$frase = "Mi manzana roja";   </span> <br>
                       <span class="text-yellow-500"> cuentaVocales</span> ($frase);<br>
                </code>


                <?php
                //echo"<h3><u>Ejercicio 24: Contador de Vocales</u></h3>";
                //Definir la funcion.
                function cuentaVocales($palabra)
                { // va a recibir una cadena (palabra)
                    $contador = 0; // para contar cuantas vocales hay

                    $vocales = array("a", "e", "i", "o", "u");

                    for ($i = 0; $i < strlen($palabra); $i++) { //strlen (corta la palabra)
                        if (in_array($palabra[$i], $vocales)) {  // accedemos a la posicion $palabra[i]
                            $contador++; //si encuentra una vocal aumenta el contador 
                        }
                    }
                    echo "El número total de vocales en la frase o palabra: " . "<u>$palabra</u> es: $contador ";
                }

                //usamos la funcion 
                $frase = "Mi manzana roja ";
                cuentaVocales($frase);
                ?>
                <div class="flex justify-center px-6 pt-4 pb-2">
                    <a href="index.html"><button class="bg-blue-400 hover:bg-blue-500 text-black font-bold py-2 px-4 rounded-full">
                            Home
                        </button>
                    </a>
                </div>
            </div>

        </div>


</body>

</html>