<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Fichero de Funciones </title>
</head>

<body class="grid grid-col-4 gap-4 p-5 justify-center m-10">
  <div class="max-w-sm rounded overflow-hidden shadow-lg bg-black text-white">
    <div class="px-6 py-4">
      <?php
      /*Entendí que el ejercicio habia que pegar todas las funciones en un fichero, pero existe la opcion de hacer el 
      require_once y solo incluirlo una vez, solo se puede llamar una vez */
      
      require_once "./Ejercicio23.php";
      ?>
      
    </div>
  </div>
</div>

<br class="my-12">

  <div class="max-w-sm rounded overflow-hidden shadow-lg bg-black text-white">
    <div class="px-6 py-4">
      <?php
      /*Entendí que el ejercicio habia que pegar todas las funciones en un fichero, pero existe la opcion de hacer el 
        require_once y solo incluirlo una vez, solo se puede llamar una vez */

      require_once "./Ejercicio24.php";
      ?>
      
    </div>
  </div>
  </div>   

  <br class="my-12">
  <div class="max-w-sm rounded overflow-hidden shadow-lg bg-black text-white">
    <div class="px-6 py-4">
      <?php
      /*Entendí que el ejercicio habia que pegar todas las funciones en un fichero, pero existe la opcion de hacer el 
        require_once y solo incluirlo una vez, solo se puede llamar una vez */

      require_once "./Ejercicio25.php";
      ?>
      
    </div>
  </div>
  </div>
  <br class="my-12">
  
  <div class="max-w-sm rounded overflow-hidden shadow-lg bg-black text-white">
    <div class="px-6 py-4">
      <?php
      /*Entendí que el ejercicio habia que pegar todas las funciones en un fichero, pero existe la opcion de hacer el 
        require_once y solo incluirlo una vez, solo se puede llamar una vez */

      require_once "./Ejercicio26.php";
      ?>
      
  
    </div>
  </div>
  </div>

</body>

</html>