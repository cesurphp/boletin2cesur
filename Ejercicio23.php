<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Ejercicio 23</title>
  <script src="https://cdn.tailwindcss.com"></script>
</head>

<body class= "flex justify-center m-10">
  <div class="max-w-sm rounded overflow-hidden shadow-lg bg-black" >
    <div class="px-6 py-4">
      <div class="font-bold text-xl mb-2 text-green-500">Ejercicio 23: Funcion media </div>
      
         <div >  
           <code >

          <span class="text-blue-500"> function <span class="text-yellow-400"> media</span> </span> <span class="text-blue-300">($num1, $num2, $num3, $num4) {
          $suma = $num1 + $num2 + $num3 + $num4; <br>
          $media = $suma / 4;<br>
          return $media; </span>
        } <br>

          <u class="text-green-500">Aplicamos la funcion</u><br>
          <span class="text-blue-300">$num1 = 5.2; <br>
          $num2 = 3.4;<br>
          $num3 = 152;<br>
          $num4 = 165;<br></span>
          <br>
          <span class="text-blue-500">$resultado =</span> <span class="text-yellow-400"> media</span> <span class="text-blue-300">($num1,$num2,$num3,$num4);<br>
          echo "La media de los 4 numeros: $resultado";</span>
        </code> 
      </div>
      
      </p>
    
    

  <?php
  //echo "<h3><u>Ejercicio 23: Funcion Media </u></h3>";
  //Definimos la funcion para llamarla desde cualquier otro ejercicio. 

  function media($num1, $num2, $num3, $num4)
  {
    $suma = $num1 + $num2 + $num3 + $num4;
    $media = $suma / 4;
    return $media;
  }

  //Aplicamos la funcion 
  $num1 = 5.2;
  $num2 = 3.4;
  $num3 = 152;
  $num4 = 165;

  //Resultado   
  $resultado = media($num1, $num2, $num3, $num4);
  echo "<p class= text-yellow-400> La media de los 4 números: $resultado</p>" ;


  ?>
  
  <div class="flex justify-center px-6 pt-4 pb-2">
      <a href="index.html"><button class="bg-blue-400 hover:bg-blue-500 text-black font-bold py-2 px-4 rounded-full">
       Home
      </button>
    </a>
    </div>
  </div>
  
</div>
</body>

</html>