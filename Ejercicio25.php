<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 25</title>
    <script src="https://cdn.tailwindcss.com"></script>
</head>
<body>
<div class="flex justify-center m-10">
        <div class="max-w-sm rounded overflow-hidden shadow-lg bg-black text-white">
            <div class="px-6 py-4">
                <div class="font-bold text-xl mb-2 text-green-500">Ejercicio 25: Funcion cuadrado</div>
                <div >
                    <code>
             
                       <span class="text-blue-300"> functionm <span class="text-yellow-500"> cuadrado </span>($caracter, $num){ <br>
                            for ($f = 0; $f < $num; $f++){  //fila <br>
                                for ($c = 0; $c < $num; $c++){ //columna  <br>
                                    <span class="text-yellow-500">echo "<strong>$caracter</strong> </span>"; <br>
                                } 
                                echo "<br>";
                            }
                        }</span>

                         <span class="text-green-500"><u>Aplicamos la funcion </u></span><br>
                         <span class="text-yellow-500"> echo "<h3>¡Mi cuadrado de Caracteres!</h3>"; 
                        cuadrado(" $ ", 4); </span>

                    
                    </code>
                

    <?php
    echo "<h3><u>Ejercicio 25: Funcion cuadrado </u></h3>";
    //Declaramos la funcion 
     /* el numero es para saber cuantos queremos 
    de caracrteres en cada columna y en cada fila*/ 
    function cuadrado ($caracter, $num,){
        for ($f=0; $f < $num; $f ++){ //fila
            for($c=0; $c < $num; $c++){ //columna 
                echo "<strong> $caracter </strong>"; // me tiene que imprimir el caracter 

            } //cuando hace una fila, realiza un salto de linea 
            echo "<br>";
        }
    }

    //usamos la funcion 
    /* echo "<h3>¡Mi cuadrado de Caracteres! </h3>"; */
      cuadrado (" $ ",4);// el caracter ""

   
    ?>
    
</div>
            </div>
            <div class="flex justify-center px-6 pt-4 pb-2">
                <a href="index.html">
                    <button class="bg-blue-400 hover:bg-blue-500 text-black font-bold py-2 px-4 rounded-full">Home</button>
                </a>
            </div>
        </div>
    </div>
</body>
</html>