<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Ejercicio 26</title>
  <script src="https://cdn.tailwindcss.com"></script>
</head>

<body class="flex justify-center m-10">
  <div class="max-w-sm rounded overflow-hidden shadow-lg bg-black text-white">
    <div class="px-6 py-4">
      <div class="font-bold text-xl mb-2 text-green-500">Ejercicio 26: Funcion lotería</div>
      <div>
        <code>
          <span class="text-blue-300">function<span class="text-yellow-500"> loteria </span> ($bolas_sacar, $bolas_totales) { <br>
            <span class="text-yellow-500">echo <u>Los números premiados son: </u> ";</span> <br>
            for ($i = 0; $i < $bolas_sacar; $i++) { <br>
              $numero = rand(1, $bolas_totales); <br>
              echo "<strong>$numero</strong>"; <br>
              if ($i < $bolas_sacar - 1) { <br>
                <span class="text-yellow-500"> echo ", "; </span> <br>
                } <br>
                } <br>
                } <br> </span>

          <span class="text-green-500"><u>Aplicamos la funcion</u> </span> <br>
          <span class="text-yellow-500">loteria</span>(6, 49);
        </code>



        <?php
          /*echo "<h3><u>Ejercicio 26: Función Loteria</u></h3>"*/;
        // hacemos la funcion 
        function loteria($bolas_sacar, $bolas_totales)
        {
          echo "<h1><u>Los números premiados son: </u></h1>";
          //para las bolas al azar 
          for ($i = 0; $i < $bolas_sacar; $i++) {
            $numero = rand(1, $bolas_totales); //generamos numeros alatorios
            echo "<strong>$numero</strong>"; // muestro numeros 

            if ($i < $bolas_sacar - 1) { //para no imprimir la ultima coma 
              echo ", ";
            }
          }
        }

        //usamos la funcion
        loteria(6, 49);
        ?>

      </div>
    </div>
    <div class="flex justify-center px-6 pt-4 pb-2">
      <a href="index.html">
        <button class="bg-blue-400 hover:bg-blue-500 text-black font-bold py-2 px-4 rounded-full">Home</button>
      </a>
    </div>
  </div>
</body>

</html>